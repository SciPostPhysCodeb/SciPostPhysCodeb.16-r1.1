# Codebase release 1.1 for LinReTraCe

by Matthias Pickem, Emanuele Maggio, Jan M. Tomczak

SciPost Phys. Codebases 16-r1.1 (2023) - published 2023-07-17

[DOI:10.21468/SciPostPhysCodeb.16-r1.1](https://doi.org/10.21468/SciPostPhysCodeb.16-r1.1)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.16-r1.1) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Matthias Pickem, Emanuele Maggio, Jan M. Tomczak

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.16-r1.1](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.16-r1.1)
* Live (external) repository at [https://github.com/LinReTraCe/LinReTraCe/tree/v1.1.7](https://github.com/LinReTraCe/LinReTraCe/tree/v1.1.7)
